﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace WDM
{
    public partial class FormWDM : Form
    {
        private MySqlConnection myConnection = null;
        private MySqlConnection myConnection2 = null;
        private DataList oData = new DataList();
        //private string config_xml = Application.StartupPath + "\\WDM.conf";
        private string data_xml = Application.StartupPath + "\\WDM.data";
        private string data_txt = Application.StartupPath + "\\WDM.txt";
        //private XDocument xDoc = null;

        public FormWDM()
        {
            InitializeComponent();
            this.Text += string.Format(" (v{0})", Application.ProductVersion);
            dataGridView_object.DataSource = oData;

            //read config
            textBox_host.Text = WDM.Properties.Settings.Default.wdm_mysqlhost;
            textBox_db.Text = WDM.Properties.Settings.Default.wdm_mysqldb;
            textBox_uname.Text = WDM.Properties.Settings.Default.wdm_mysqluser;
            textBox_pass.Text = WDM.Properties.Settings.Default.wdm_mysqlpass;
            textBox_port.Text = WDM.Properties.Settings.Default.wdm_mysqlport.ToString();

            //read data
            if (File.Exists(data_xml))
                oData.Load(data_xml);
        }

        private delegate void AppendTextBoxTextDelegate(string text);
        private void LogText(string log)
        {
            string log_ = "[" + String.Format("{0:D2}:{1:D2}:{2:D2}", DateTime.Now.Hour, DateTime.Now.Minute, +DateTime.Now.Second) + "] " + log + Environment.NewLine;
            if (this.textBox_Logs.InvokeRequired)
            {
                // This is a worker thread so delegate the task.
                this.textBox_Logs.Invoke(new AppendTextBoxTextDelegate(this.textBox_Logs.AppendText), log_);
            }
            else
            {
                // This is the UI thread so perform the task.
                this.textBox_Logs.AppendText(log_);
            } 
        }

         private void LogText(string format, params object[] args)
        {
            LogText(String.Format(format, args));
        }

         private void SQLConnect(object sender, DoWorkEventArgs e)
        {
            string sql_host = textBox_host.Text;
            string sql_uname = textBox_uname.Text;
            string sql_pass = textBox_pass.Text;
            string sql_db = textBox_db.Text;
            int sql_port = 0;
            if (!int.TryParse(textBox_port.Text, out sql_port)) sql_port = 3306;
            LogText("Open MySQL connection to {0}@{1}:{2}.", sql_uname, sql_host, sql_port);

            //try
            {
                myConnection = new MySqlConnection(
                    "SERVER=" + sql_host + ";" +
                    "DATABASE=" + sql_db + "; " +
                    "UID=" + sql_uname + ";" +
                    "PASSWORD=" + sql_pass + ";");
                myConnection.Open();
                myConnection2 = new MySqlConnection(
                    "SERVER=" + sql_host + ";" +
                    "DATABASE=" + sql_db + "; " +
                    "UID=" + sql_uname + ";" +
                    "PASSWORD=" + sql_pass + ";");
                myConnection2.Open();
                LogText("Connected.");
                MySqlCommand cmdMySQL = myConnection.CreateCommand();
                MySqlCommand cmdMySQL2 = myConnection2.CreateCommand();
                MySqlDataReader reader;
                MySqlDataReader reader2;
                StreamWriter sW = File.CreateText(data_txt);

                foreach (WDMObjectData data in oData)
                {
                    LogText("Object Id {0} : \"{1}\".", data.Id, data.Caption);
                    cmdMySQL.CommandText = "select map, position_x, position_y, position_z, spawntimesecs, guid from gameobject where gameobject.id = " + data.Id + " order by map, spawntimesecs asc";

                    /*
                    cmdMySQL.CommandText = "select map, position_x, position_y, position_z, spawntimesecs, pool_template.entry, pool_gameobject.chance, pool_template.max_limit, pool_template.description, gameobject_template.name " +
                    " from " +
                    "gameobject, pool_template, pool_gameobject, gameobject_template " +
                    "where " +
                    "gameobject_template.entry = gameobject.id AND " +
                    "pool_gameobject.guid = gameobject.guid AND " +
                    "pool_template.entry = pool_gameobject.pool_entry AND " +
                    "gameobject.id = " + data.Id;// + gak perlu order-by, ntar keurut sendiri di weh
                    //" order by map, spawntimesecs asc";
                    */
                    reader = cmdMySQL.ExecuteReader();

                    uint count = 0;
                    while (reader.Read())
                    {
                        count++;

                        int tMapId = (int)reader.GetInt32(0);
                        float tXPos = (float)reader.GetFloat(1);
                        float tYPos = (float)reader.GetFloat(2);
                        float tZPos = (float)reader.GetFloat(3);
                        int tSpawnTS = (int)reader.GetInt32(4);
                        int tGuid = (int)reader.GetInt32(5);
                        //int tEntry = (int)reader.GetInt32(5);
                        //int tSpawnChance = (int)reader.GetInt32(6);
                        //int tSpawnLimit = (int)reader.GetInt32(7);
                        //string tODesc = (string)reader.GetString(8);
                        //string tOName = (string)reader.GetString(9);

                        int tSpawnLimit = 0;
                        string tODesc = "";
                        cmdMySQL2.CommandText = "select pool_template.max_limit, pool_gameobject.description from pool_template, pool_gameobject where pool_gameobject.pool_entry = pool_template.entry and pool_gameobject.guid = " + tGuid;
                        reader2 = cmdMySQL2.ExecuteReader();
                        while (reader2.Read())
                        {
                            tSpawnLimit = (int)reader2.GetInt32(0);
                            tODesc = (string)reader2.GetString(1);
                            break;
                        }
                        reader2.Close();

                        //betulin text desc
                        string[] tODescSplit = tODesc.Split(' ');
                        string tFODesc  = "";
                        foreach (string s in tODescSplit)
                        {
                            int tNum = 0;
                            if (Int32.TryParse(s, out tNum))
                                tFODesc = tFODesc + string.Format("{0:D3}", tNum) + " ";
                            else
                                tFODesc = tFODesc + s + " ";
                        }
                        tFODesc = tFODesc.Trim();

                        if (tSpawnLimit > 0)
                            sW.WriteLine("[" + data.Caption + " " + tMapId + ":(" + tFODesc + ", max:" + tSpawnLimit + "):" + tSpawnTS + "s:" + string.Format("{0:D4}", count) + "]");
                        else
                            sW.WriteLine("[" + data.Caption + " " + tMapId + ":" + tSpawnTS + "s:" + string.Format("{0:D4}", count) + "]");
                        //sW.WriteLine("[" + data.Caption + " " + tMapId + ":e" + string.Format("{0:D4}", tEntry) + "(" + tFODesc + " %" + tSpawnChance + "/" + tSpawnLimit + "):" + tSpawnTS + "s:" + string.Format("{0:D4}", count) + " - " + tOName + "]");
                        sW.WriteLine("Type:=Object");
                        sW.WriteLine("MapID:=" + tMapId);
                        sW.WriteLine("XPos:=" + tYPos); //switch x-y
                        sW.WriteLine("YPos:=" + tXPos); //switch x-y
                        sW.WriteLine("ZPos:=" + tZPos);
                        sW.WriteLine("Comment:=WoW Data Mining");
                        sW.WriteLine();
                    }

                    reader.Close();
                    LogText("Done ({0}).", data.Id);
                }

                LogText("All Done.");
                sW.Close();
                myConnection.Close();
                LogText("MySQL connection closed.");
                this.Invoke((Action)delegate { this.button_Connect.Text = "Done."; });
            }
            //catch (Exception exc)
            {
                //LogText(exc.ToString());
                //this.Invoke((Action)delegate { this.button_Connect.Enabled = true; });
            }
        }

        private void button_Do_Click(object sender, EventArgs e)
        {
            button_Connect.Enabled = false;
            oData.Save(data_xml);

            //save config
            WDM.Properties.Settings.Default.wdm_mysqlhost = textBox_host.Text;
            WDM.Properties.Settings.Default.wdm_mysqldb = textBox_db.Text;
            WDM.Properties.Settings.Default.wdm_mysqluser = textBox_uname.Text;
            WDM.Properties.Settings.Default.wdm_mysqlpass = textBox_pass.Text;
            WDM.Properties.Settings.Default.Save();
            //Settings1.Default.wdm_mysqlport = uint.Parse(textBox_port.Text);

            backgroundWorker1.RunWorkerAsync();
        }
    }

    public class WDMObjectData
    {
        private uint id;
        private string caption;

        public uint Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Caption
        {
            get { return caption; }
            set { caption = value; }
        }
    }

    public class DataList : BindingList<WDMObjectData>
    {
        public void AddRange(DataList events)
        {
            foreach (WDMObjectData customEvent in events)
                this.Add(customEvent);
        }

        public void Load(string xmlFile)
        {
            try
            {
                XDocument doc = XDocument.Load(xmlFile);

                var query = from xElem in doc.Descendants("Object")
                            select new WDMObjectData
                            {
                                Id = uint.Parse(xElem.Attribute("Id").Value),
                                Caption = xElem.Element("Caption").Value,
                            };

                this.Clear();
                //AddRange(query);
                foreach (WDMObjectData wd in query)
                {
                    Add(wd);
                }
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
        }

        public void Save(string xmlFile)
        {
            if (this.Count == 0) return;

            try
            {
                XElement xml = new XElement("ObjectData",
                                from p in this
                                select new XElement("Object",
                                    new XAttribute("Id", p.Id),
                                    new XElement("Caption", p.Caption)));
                xml.Save(xmlFile);
            }
            catch (Exception e)
            {
                System.Console.WriteLine(e);
            }
        }
    }
}
